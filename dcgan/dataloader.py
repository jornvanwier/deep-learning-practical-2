import numpy as np
import pandas as pd
from skimage import io, transform
from PIL import Image

import torch, os
import torchvision
import torchvision.datasets as dset
from torchvision import transforms
from torch.utils.data import DataLoader, Dataset


class AttributeDataset(Dataset):

    def __init__(self, csv_file, img_dir, transform = None):
        self.df = pd.read_csv(csv_file)
        self.df = self.df[["image_id", "Attractive", "High_Cheekbones", "Male", "Smiling", "Wearing_Lipstick"]]
        self.img_dir = img_dir
        self.transform = transform
        self.Tensor = torch.FloatTensor
        self.i = 0
    
    def __len__(self):
        return len(self.df)

    def __getitem__(self, idx):
        img_name = self.df.iloc[idx]['image_id']
        img_path = f"{self.img_dir}/{img_name}"

        if self.transform:
            image = Image.open(img_path)
        else:
            image = io.imread(img_path)

        att = self.df.iloc[idx, 1:]
        att = np.array(att)
        att = att.astype('float')
        att = self.Tensor(att)
        att = att.view(att.size(0), 1, 1)

        z = torch.randn(100, 1, 1)

        if self.transform:
            image = self.transform(image)
        
        sample = {'image': image, 'att': att, 'z': z}
        
        return sample