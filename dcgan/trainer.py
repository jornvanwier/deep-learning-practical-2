import torch
import torch.nn.functional as F
import torch.autograd as autograd
from torchvision.utils import save_image
from torch.autograd import Variable

import time
import datetime
import os

from network import Generator, Discriminator, weights_init


class trainer(object):

    def __init__(self, 
                epochs,
                dataloader,
                resume_epoch,
                model_save_dir,
                image_dir,
                batch_size,
                d_lr = 0.0002,
                g_lr = 0.0002,
                device = 'cpu',
                nl = 5):

        self.epochs = epochs
        self.dl = dataloader
        self.resume_epoch = resume_epoch
        self.device = device
        self.bsize = batch_size

        self.model_save_dir = model_save_dir
        self.image_dir = image_dir

        self.d_lr = d_lr
        self.g_lr = g_lr

        self.g = Generator().to(self.device)
        self.g.apply(weights_init)

        self.d = Discriminator(nl = nl).to(self.device)
        self.d.apply(weights_init)

        self.g_opt = torch.optim.Adam(self.g.parameters(), lr=self.g_lr, betas=(0.5, 0.999))
        self.d_opt = torch.optim.Adam(self.d.parameters(), lr=self.d_lr, betas=(0.5, 0.999))

        self.criterion = torch.nn.BCELoss()

        self.log_step = 1
        self.model_step = 1
        self.save_step = 1
    
    def denorm(self, x):
        out = (x + 1) / 2
        return out.clamp_(0, 1)
    
    def restore_model(self, res_epoch):
        print('Loading the trained models from epoch {} ...'.format(res_epoch))
        G_path = os.path.join(self.model_save_dir, '{}-G.ckpt'.format(res_epoch))
        D_path = os.path.join(self.model_save_dir, '{}-D.ckpt'.format(res_epoch))
        self.g.load_state_dict(torch.load(G_path, map_location=lambda storage, loc: storage))
        self.d.load_state_dict(torch.load(D_path, map_location=lambda storage, loc: storage))
    
    def train(self):
        
        data_iter = iter(self.dl)
        data = next(data_iter)
        att = data["att"]
        z = data["z"]
        x_fixed = torch.cat((z, att), dim = 1).to(self.device)

        start_epoch = 0
        
        if self.device == "cuda":
            Tensor = torch.cuda.FloatTensor
        else:
            Tensor = torch.FloatTensor

        if self.resume_epoch:
            start_epoch = self.resume_epoch
            self.restore_model(self.resume_epoch)

        start_time = time.time() 
        
        for epoch in range(start_epoch, self.epochs):

            for i, data in enumerate(self.dl):

                img = data["image"].to(self.device)
                att = data["att"].to(self.device)
                z = data["z"].to(self.device)

                x = torch.cat((z, att), dim = 1).to(self.device)
                valid = Variable(Tensor(x.size(0),).fill_(1.0), requires_grad=False)
                fake = Variable(Tensor(x.size(0)).fill_(0.0), requires_grad=False)

                self.g_opt.zero_grad()
                gen_imgs = self.g(x)
                g_loss = self.criterion(self.d(gen_imgs, att).view(-1), valid)
                g_loss.backward()
                self.g_opt.step()

                self.d_opt.zero_grad()
                real_loss = self.criterion(self.d(img, att).view(-1), valid)
                fake_loss = self.criterion(self.d(gen_imgs.detach(), att).view(-1), fake)
                torch.autograd.set_detect_anomaly(True)
                d_loss = (real_loss + fake_loss)/2
                d_loss.backward()
                self.d_opt.step()


                if (i+1) % self.log_step == 0:
                    et = time.time() - start_time
                    et = str(datetime.timedelta(seconds=et))[:-7]
                    print('Elapsed [{}]\t[{}/{}][{}/{}]\tLoss_D: {:.4f}\tLoss_G: {:.4f}'
                            .format(et, epoch + 1, self.epochs, i+1, len(self.dl),
                            d_loss.item(), g_loss.item()))

            if (epoch+1) % self.model_step == 0:
                G_path = os.path.join(self.model_save_dir, '{}-G.ckpt'.format(epoch+1))
                D_path = os.path.join(self.model_save_dir, '{}-D.ckpt'.format(epoch+1))
                torch.save(self.g.state_dict(), G_path)
                torch.save(self.d.state_dict(), D_path)
                print('Saved model checkpoints into {}...'.format(self.model_save_dir))
            
            if (epoch+1) % self.save_step == 0:
                with torch.no_grad():
                    img = self.g(x_fixed)
                    sample_path = os.path.join(self.image_dir, '{}-image.jpg'.format(epoch+1))
                    save_image(self.denorm(img[0].data.cpu()), sample_path, nrow=1, padding=0)
                    print('Saved image into {}...'.format(sample_path))