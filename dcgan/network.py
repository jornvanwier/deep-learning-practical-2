import torch
import torch.nn as nn
import torch.nn.functional as F

from skimage import io
from skimage.transform import rescale, resize, downscale_local_mean
from PIL import Image
import numpy as np
import matplotlib.pyplot as plt


def weights_init(m):
    classname = m.__class__.__name__
    if classname.find('Conv') != -1:
        nn.init.normal_(m.weight.data, 0.0, 0.02)
    elif classname.find('BatchNorm') != -1:
        nn.init.normal_(m.weight.data, 1.0, 0.02)
        nn.init.constant_(m.bias.data, 0)


class Generator(nn.Module):
    def __init__(self, ngpu = 1, nz = 105, ngf = 64, nc = 3):
        super(Generator, self).__init__()
        self.ngpu = ngpu
        self.main = nn.Sequential(
            nn.ConvTranspose2d( nz, ngf * 8, 4, 1, 0, bias=False),
            nn.BatchNorm2d(ngf * 8),
            nn.ReLU(True),
            
            nn.ConvTranspose2d(ngf * 8, ngf * 4, 4, 2, 1, bias=False),
            nn.BatchNorm2d(ngf * 4),
            nn.ReLU(True),
            
            nn.ConvTranspose2d( ngf * 4, ngf * 2, 4, 2, 1, bias=False),
            nn.BatchNorm2d(ngf * 2),
            nn.ReLU(True),
            
            nn.ConvTranspose2d( ngf * 2, ngf, 4, 2, 1, bias=False),
            nn.BatchNorm2d(ngf),
            nn.ReLU(True),
            
            nn.ConvTranspose2d( ngf, nc, 4, 2, 1, bias=False),
            nn.Tanh()
        )


    def forward(self, input):
        return self.main(input)

class Discriminator(nn.Module):
    def __init__(self, nl, ngpu = 1, nc = 3, nf = 5, ndf = 64):
        super(Discriminator, self).__init__()
        self.ngpu = ngpu
        
        self.first_layer = nn.Sequential(
            nn.Conv2d(nc, ndf, 4, 2, 1, bias=False),
            nn.LeakyReLU(0.2, inplace=True))
        
        self.second_layer = nn.Sequential(
            nn.Conv2d(ndf + nf, ndf * 2, 4, 2, 1, bias=False),
            nn.BatchNorm2d(ndf * 2),
            nn.LeakyReLU(0.2, inplace=True),
        )

        self.main = nn.Sequential(
            nn.Conv2d(ndf * 2 + nf, ndf * 4, 4, 2, 1, bias=False),
            nn.BatchNorm2d(ndf * 4),
            nn.LeakyReLU(0.2, inplace=True),
            
            nn.Conv2d(ndf * 4, ndf * 8, 4, 2, 1, bias=False),
            nn.BatchNorm2d(ndf * 8),
            nn.LeakyReLU(0.2, inplace=True),
            
            nn.Conv2d(ndf * 8, 1, 4, 1, 0, bias=False),
            nn.Sigmoid())

    def forward(self, x, l):
        l2 = l

        x = self.first_layer(x)
        l = l.view(l.size(0), l.size(1), 1, 1)
        l = l.repeat(1, 1, x.size(2), x.size(3))
        x = torch.cat((x, l), 1)

        x = self.second_layer(x)
        l2 = l2.view(l2.size(0), l2.size(1), 1, 1)
        l2 = l2.repeat(1, 1, x.size(2), x.size(3))
        x = torch.cat((x, l2), 1)

        return self.main(x)