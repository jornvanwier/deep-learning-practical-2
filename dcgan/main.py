from trainer import trainer
from dataloader import AttributeDataset

import torch
from torch.utils.data import DataLoader
from torchvision import transforms

import os

epochs = 200
batchsize = 16
csv_path = 'data/list_attr_celeba.csv'
img_dir = 'data/img_align_celeba/img_align_celeba'
model_save_dir = 'models'
sample_dir = 'results'

image_size = 64
center_crop = 178
transform = transforms.Compose([
    transforms.CenterCrop(center_crop),
    transforms.Resize(image_size),
    transforms.ToTensor(),
    transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))
])

df = AttributeDataset(csv_path, img_dir, transform = transform)
dl = DataLoader(df, batch_size=batchsize, shuffle=True, num_workers=0)
device = 'cuda' if torch.cuda.is_available() else 'cpu'

tot_batch = len(df) // batchsize

ckpt =  os.listdir(model_save_dir)

if len(ckpt) > 0:
    ckpt = ckpt[-1]
    l_epoch = int(ckpt.split('-')[0])
    l_batch = None
else:
    l_epoch = None
    l_batch = None

tr = trainer(epochs, 
            dl,
            l_epoch,  
            model_save_dir, 
            sample_dir, 
            batchsize,
            device = device)
tr.train()