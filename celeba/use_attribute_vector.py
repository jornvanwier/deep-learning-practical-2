import matplotlib.pyplot as plt
import numpy as np
import torch

from celeba.generation import generate_with_attributes, generate_img, load_attribute
from celeba.utils import select_device, load_generator, GENERATOR_PKL


def main():
    device = select_device()

    generator = load_generator(device, GENERATOR_PKL)

    # generate_spectrum(generator, device, 'Bangs')

    generate_grid(generator,
                  device,
                  'Attractive',
                  signs=[1]
                  )

    # stupid plots
    plt.show()


def generate_grid(generator, device, *attribute_names, signs=None, width=6, height=1):
    n = width * height

    images, signs = generate_with_attributes(generator, device, n, attribute_names, signs)

    label = ' + '.join(prefix + attribute for attribute, prefix in
                       ((attribute, f'{"-" if sign < 0 else ""}{abs(sign) if abs(sign) != 1 else ""}')
                        for attribute, sign in zip(attribute_names, signs)))

    figure = plt.figure(None, (width * 3, height * 3))
    axs = figure.subplots(height, width)
    figure.suptitle(label)
    plt.tight_layout()

    for ax, img in zip(axs.flatten(), images):
        ax.axis('off')
        ax.imshow(img[0].cpu())


def generate_spectrum(generator, device, attribute_name):
    latent_direction = load_attribute(attribute_name, device)

    z = torch.from_numpy(np.random.RandomState().randn(1, generator.z_dim)).to(device)
    # weights = [-2, -1, 0, 1, 2]
    # weights = [0, 1, 2]
    weights = [-1, -.5, 0, .5, 1]
    figure = plt.figure(None, (3, len(weights) * 2))
    axs = figure.subplots(len(weights))
    plt.tight_layout()
    for weight, ax in zip(weights, axs):
        zw = z + weight * latent_direction

        img = generate_img(generator, device, zw)

        ax.set_title(f'{weight} * {attribute_name}')
        ax.imshow(img[0].cpu())


if __name__ == '__main__':
    main()
