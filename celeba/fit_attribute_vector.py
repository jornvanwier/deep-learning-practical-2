from collections import Counter

import numpy as np
import torch
from sklearn.linear_model import LogisticRegression
from tqdm import tqdm

from celeba.utils import ATTR_NAMES, load_generator, GENERATOR_PKL
from celeba.classifier.utils import load_classifier, PREPROCESS_TRANSFORM


def main():
    device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')

    classifier, generator = load_models(device)

    # find_balanced_attributes(classifier, generator)

    find_latent_directions(classifier, generator, device, *ATTR_NAMES)


def find_latent_directions(classifier: torch.nn.Module, generator: torch.nn.Module, device: torch.device,
                           *attributes: str):
    n = 30_000
    # n = 5_000

    print('Finding latent directions for', attributes)

    with torch.no_grad():
        latent_gen = tqdm((generate_latent_label_pair(classifier, generator, n, device)), 'Image generator', total=n)
        x, y_all = zip(*((z.cpu().numpy().flatten(), preds) for z, _, preds in latent_gen))
        x, y_all = map(list, (x, y_all))
    assert len(x) == n, len(y_all) == n

    for attribute in attributes:
        idx = ATTR_NAMES.index(attribute)
        y = [preds[idx].item() for preds in y_all]

        try:
            # fit_attribute(attribute, n, x, y)
            fit_attribute_balanced(attribute, n, x, y)
        except ValueError as e:
            print(f'Skipping {attribute}: {e}')


def fit_attribute(attribute, n, x, y):
    class_counter = Counter(y)
    if len(class_counter) == 1:
        raise ValueError(f'{attribute}={not next(iter(class_counter))} never occurred in {n} samples')
    for value, count in class_counter.items():
        print(f'{value}: {count}')
        if count <= n // 10:
            print(
                f'{attribute}={value} is quite rare, consider increasing sample size or choosing another attribute')

    np.reshape(x, (-1,))
    print(f'Fitting attribute {attribute} on {n} samples')
    model = LogisticRegression(class_weight='balanced').fit(x, y)
    latent_vector = model.coef_
    np.savez(f'out/attribute_vectors/{attribute}.npz', latent_vector)


def fit_attribute_balanced(attribute, n, x, y):
    class_counter = Counter(y)
    if len(class_counter) == 1:
        raise ValueError(f'{attribute}={not next(iter(class_counter))} never occurred in {n} samples')

    least_common_value, least_common_count = class_counter.most_common()[-1]
    y = np.array(y)
    keep = np.where(y == least_common_value)[0]
    subset = np.where(y == (not least_common_value))[0]

    rng = np.random.default_rng()
    indices = np.concatenate((keep, rng.choice(subset, least_common_count, replace=False)))

    x = np.array(x)[indices]
    y = y[indices]

    assert len(x) == len(y)

    np.reshape(x, (-1,))
    print(f'Fitting attribute {attribute} on {len(x)} balanced samples')
    model = LogisticRegression(class_weight='balanced').fit(x, y)
    latent_vector = model.coef_
    np.savez(f'out/attribute_vectors/{attribute}.npz', latent_vector)


def find_balanced_attributes(classifier, generator, device):
    n = 1_000

    preds = np.array([p for _, _, p in tqdm(generate_latent_label_pair(classifier, generator, n, device))])
    sums = np.sum(1 * preds, 0)
    ratios = sums / n

    for label, ratio in zip(ATTR_NAMES, ratios):
        print(f'{label},{ratio:.2f}')


def load_models(device):
    classifier = load_classifier(device)
    generator = load_generator(device, GENERATOR_PKL)
    return classifier, generator


def generate_latent_label_pair(classifier, generator, n, device):
    batch_size = 32

    for i in range((n // batch_size) + 1):
        progress = i * batch_size
        current_batch = batch_size if progress + batch_size < n else n - progress

        if current_batch == 0:
            return

        z = torch.from_numpy(np.random.RandomState().randn(current_batch, generator.z_dim)).to(device)
        raw_img = generator(z, torch.zeros([current_batch, generator.c_dim], device=device), truncation_psi=1,
                            noise_mode='const')
        img = (raw_img.permute(0, 2, 3, 1) * 127.5 + 128).clamp(0, 255).to(torch.uint8)
        # preprocess for classifier
        classifier_img = PREPROCESS_TRANSFORM(raw_img)
        outputs = classifier(classifier_img)
        preds = (outputs > .9)

        for j in range(current_batch):
            yield z[j], img[j], preds[j]
    # return z, img, preds
    return


if __name__ == '__main__':
    main()
