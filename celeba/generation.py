from typing import Iterable, Optional, Tuple, List

import numpy as np
import torch


def generate_with_attributes(generator: torch.nn.Module, device: torch.device, n: int, attribute_names: Iterable[str],
                             signs: Optional[Iterable[int]]) -> Tuple[List[torch.Tensor], Iterable[int]]:
    latent_directions = [load_attribute(attribute, device) for attribute in attribute_names]

    if signs is None:
        signs = np.ones(len(latent_directions))
    assert len(signs) == len(latent_directions)

    images = [generate_image_with_latent(device, generator, latent_directions, signs) for _ in range(n)]

    return images, signs


def generate_img(generator, device, z):
    raw_img = generator(z, torch.zeros([1, generator.c_dim], device=device), truncation_psi=1,
                        noise_mode='const')
    img = (raw_img.permute(0, 2, 3, 1) * 127.5 + 128).clamp(0, 255).to(torch.uint8)

    return img


def load_attribute(attribute, device):
    with np.load(f'out/attribute_vectors/{attribute}.npz') as file:
        latent_direction = torch.tensor(file[file.files[0]], device=device)
    return latent_direction


def generate_image_with_latent(device, generator, latent_directions, signs):
    z = merge_z_with_attributes(device, generator.z_dim, latent_directions, signs)

    img = generate_img(generator, device, z)
    return img


def merge_z_with_attributes(device, z_dim, latent_directions, signs, zw=.5):
    z = torch.from_numpy(np.random.RandomState().randn(1, z_dim)).to(device)

    # # TODO How to combine attribute without exploding
    merged = torch.zeros((1, z_dim), device=device)
    for weight, vector in zip(signs, latent_directions):
        merged += weight * vector

    z = z * zw + merged
    z /= sum(abs(sign) for sign in signs) + zw

    return z
