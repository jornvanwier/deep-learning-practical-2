import torch

import celeba.classifier.utils as classifier_utils
from celeba import generation
from celeba import utils
from celeba.utils import GENERATOR_PKL

N = 1_000
BATCH_SIZE = 128


def main():
    device = utils.select_device()

    classifier = classifier_utils.load_classifier(device)

    # Replace for other GANs
    generator = utils.load_generator(device, GENERATOR_PKL)

    with torch.no_grad():
        for idx, attr in enumerate(utils.ATTR_NAMES):
            corrects = 0
            seen = 0

            try:
                latent = generation.load_attribute(attr, device)
            except FileNotFoundError:
                print(f'{attr}\tn/a')
                continue

            zs = [generation.merge_z_with_attributes(device, generator.z_dim, latent, [1]) for _ in range(N)]

            for i in range((N // BATCH_SIZE) + 1):
                progress = i * BATCH_SIZE
                current_batch = BATCH_SIZE if progress + BATCH_SIZE < N else N - progress

                if current_batch == 0:
                    return

                z_batch = zs[i:i+current_batch]
                z = torch.cat(z_batch)
                raw_img = generator(z, torch.zeros([current_batch, generator.c_dim], device=device), truncation_psi=1,
                                    noise_mode='const')
                # preprocess for classifier
                classifier_img = classifier_utils.PREPROCESS_TRANSFORM(raw_img)
                outputs = classifier(classifier_img)
                preds = (outputs > .9)

                for features in preds:
                    seen += 1
                    if features[idx]:
                        corrects += 1

            # print(f'{attr}: {corrects * 100 / N :.2f}%')
            print(f'{attr}\t{corrects / N :.2f}')


if __name__ == '__main__':
    main()
