import time

import torch
from torch import nn, optim
from torch.optim import lr_scheduler
from tqdm import tqdm

from celeba.classifier.eval import visualize_model
from celeba.classifier.utils import create_classifier, create_datasets


def train_model(model, criterion, optimizer, scheduler, dataloaders, dataset_sizes, n_attr, num_epochs=25):
    since = time.time()

    best_acc = 0.0

    for epoch in range(num_epochs):
        tqdm.write('Epoch {}/{}'.format(epoch, num_epochs - 1))
        tqdm.write('-' * 10)

        # Each epoch has a training and validation phase
        for phase in ['train', 'valid']:
            if phase == 'train':
                model.train()  # Set model to training mode
            else:
                model.eval()  # Set model to evaluate mode

            running_loss = 0.0
            running_corrects = 0

            # Iterate over data.
            for inputs, labels in tqdm(dataloaders[phase], phase):
                inputs = inputs.to(device)
                labels = labels.to(device).float()

                # zero the parameter gradients
                optimizer.zero_grad()

                # forward
                # track history if only in train
                with torch.set_grad_enabled(phase == 'train'):
                    outputs = model(inputs)
                    # _, preds = torch.max(outputs, 1)
                    preds = outputs > .9
                    loss = criterion(outputs, labels)

                    # backward + optimize only if in training phase
                    if phase == 'train':
                        loss.backward()
                        optimizer.step()

                # statistics
                running_loss += loss.item() * inputs.size(0)
                running_corrects += torch.sum(preds == labels.data) / n_attr
            if phase == 'train':
                scheduler.step()

            epoch_loss = running_loss / dataset_sizes[phase]
            epoch_acc = running_corrects.double() / dataset_sizes[phase]

            tqdm.write('{} Loss: {:.4f} Acc: {:.4f}'.format(
                phase, epoch_loss, epoch_acc))

            # deep copy the model
            if phase == 'valid' and epoch_acc > best_acc:
                best_acc = epoch_acc
                torch.save(model.state_dict(), '../model.pt')

        tqdm.write('')

    time_elapsed = time.time() - since
    tqdm.write('Training complete in {:.0f}m {:.0f}s'.format(
        time_elapsed // 60, time_elapsed % 60))
    tqdm.write('Best val Acc: {:4f}'.format(best_acc))

    # load best model weights
    model.load_state_dict(torch.load('model.pt'))
    return model


def main():
    dataloaders, image_datasets = create_datasets()

    dataset_sizes = {x: len(image_datasets[x]) for x in ['train', 'valid']}
    n_attrs = image_datasets['train'].attr.size()[1]

    model_ft = create_classifier(n_attrs)

    model_ft = model_ft.to(device)

    criterion = nn.BCEWithLogitsLoss()
    # criterion = nn.MSELoss()

    # Observe that all parameters are being optimized
    # optimizer_ft = optim.Adam(model_ft.parameters(), lr=0.001)
    optimizer_ft = optim.Adam(model_ft.parameters(), lr=0.001)

    # Decay LR by a factor of 0.1 every 7 epochs
    exp_lr_scheduler = lr_scheduler.StepLR(optimizer_ft, step_size=7, gamma=0.1)

    trained = train_model(model_ft, criterion, optimizer_ft, exp_lr_scheduler, dataloaders, dataset_sizes, n_attrs)

    visualize_model(trained, dataloaders['valid'], image_datasets['train'].attr_names)


if __name__ == '__main__':
    device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')

    main()
