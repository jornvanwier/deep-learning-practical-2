import torch
from torch import nn
from torchvision import models
from torchvision.transforms import transforms

from celeba.classifier.dataset import CelebA


def load_classifier(device: torch.device):
    classifier = create_classifier(40)
    # classifier.load_state_dict(torch.load('model.pt'))
    # classifier.load_state_dict(torch.load('peregrine.pt'))
    # classifier.load_state_dict(torch.load('peregrine2.pt'))
    # classifier.load_state_dict(torch.load('peregrine3.pt'))
    classifier.load_state_dict(torch.load('peregrine4.pt'))
    classifier.eval()
    classifier.to(device)
    return classifier


def create_classifier(n_attrs):
    model_ft = models.mobilenet_v2(pretrained=True)
    # Here the size of each output sample is set to 2.
    # Alternatively, it can be generalized to nn.Linear(num_ftrs, len(class_names)).
    model_ft.classifier = nn.Sequential(
        nn.Dropout(0.2),
        nn.Linear(model_ft.last_channel, n_attrs)
    )
    return model_ft


def create_datasets():
    eval_transform = transforms.Compose([
        transforms.Resize(256),
        transforms.CenterCrop(224),
        transforms.ToTensor(),
        transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225]),
    ])
    data_transforms = {
        'train': transforms.Compose([
            transforms.RandomResizedCrop(224),
            transforms.RandomHorizontalFlip(),
            transforms.ToTensor(),
            transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225]),
        ]),
        'valid': eval_transform,
        'test': eval_transform,
    }
    splits = ['train', 'valid', 'test']
    image_datasets = {split: CelebA('~/University/Deep Learning/', split=split, transform=data_transforms[split])
                      for split in splits}
    dataloaders = {x: torch.utils.data.DataLoader(image_datasets[x], batch_size=4,
                                                  shuffle=True, num_workers=4)
                   for x in splits}
    return dataloaders, image_datasets


PREPROCESS_TRANSFORM = transforms.Compose([
    transforms.Resize(256),
    transforms.CenterCrop(224),
    transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
])