import operator

import torch
from tqdm import tqdm

import celeba.classifier.utils
from celeba import utils


def test(model, test_loader, n):
    model.eval()

    running_corrects = 0
    class_corrects = {attr_name: (0, 0, 0, 0, 0) for attr_name in utils.ATTR_NAMES}

    with torch.no_grad():
        for inputs, labels in tqdm(test_loader, 'Test'):
            inputs = inputs.to(device)
            labels = labels.to(device).float()

            outputs = model(inputs)
            preds = outputs > .9
            running_corrects += torch.sum(preds == labels.data) / len(utils.ATTR_NAMES)

            for label, pred in zip(labels, preds):
                for i, (class_target, class_pred) in enumerate(zip(label, pred)):
                    correct, true_pos, false_pos, false_neg, true_neg = class_corrects[utils.ATTR_NAMES[i]]
                    if class_target == class_pred:
                        correct += 1
                        if class_target:
                            true_pos += 1
                        else:
                            true_neg += 1

                    elif class_target:
                        false_neg += 1
                    else:
                        false_pos += 1
                    class_corrects[utils.ATTR_NAMES[i]] = correct, true_pos, false_pos, false_neg, true_neg

        print(f'Overall accuracy: {running_corrects.item() * 100 / n:.2f}')
        print('Class,Accuracy,True Positive Rate,False Positive Rate,False Negative Rate,True Negative Rate')
        for label, (correct, true_pos, false_pos, false_neg, true_neg) in sorted(class_corrects.items(),
                                                                                 key=operator.itemgetter(1)):
            assert true_pos + false_pos + false_neg + true_neg == n
            assert correct == n - (false_pos + false_neg)
            print(f'{label},{correct / n:.2f},'
                  f'{true_pos / n:.2f},{false_pos / n:.2f},'
                  f'{false_neg / n:.2f},{true_neg / n:.2f}')


def main():
    model = celeba.classifier.utils.create_classifier(40)
    model.load_state_dict(torch.load('peregrine4.pt'))
    model.to(device)

    data_loaders, image_datasets = celeba.classifier.utils.create_datasets()
    test_loader = data_loaders['test']

    test(model, test_loader, len(image_datasets['test']))


if __name__ == '__main__':
    device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')

    main()
