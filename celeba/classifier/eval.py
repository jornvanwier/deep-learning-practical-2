import numpy as np
import torch
from matplotlib import pyplot as plt

from celeba.classifier.utils import create_classifier, create_datasets


def imshow(inp, title=None):
    """Imshow for Tensor."""
    inp = inp.numpy().transpose((1, 2, 0))
    mean = np.array([0.485, 0.456, 0.406])
    std = np.array([0.229, 0.224, 0.225])
    inp = std * inp + mean
    inp = np.clip(inp, 0, 1)
    plt.imshow(inp)
    if title is not None:
        plt.title(title)
    plt.pause(0.001)  # pause a bit so that plots are updated


def visualize_model(model, loader, attr_names, num_images=6):
    was_training = model.training
    model.eval()
    images_so_far = 0

    with torch.no_grad():
        for i, (inputs, labels) in enumerate(loader):
            inputs = inputs.to(device)

            outputs = model(inputs)
            # preds = outputs.round()
            preds = (outputs > .8)

            for j in range(inputs.size()[0]):
                images_so_far += 1

                labels = [attr_names[i] for i, v in enumerate(preds[j]) if v]
                plt.title(f'{images_so_far}: {len(labels)} labels')
                print('labels', images_so_far, labels)
                imshow(inputs.cpu().data[j])

                if images_so_far == num_images:
                    model.train(mode=was_training)
                    return
        model.train(mode=was_training)


def main():
    model = create_classifier(n_attrs=40).to(device)
    data_loaders, image_datasets = create_datasets()
    model.load_state_dict(torch.load('peregrine.pt'))
    visualize_model(model, data_loaders['valid'], image_datasets['train'].attr_names)


if __name__ == '__main__':
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

    main()