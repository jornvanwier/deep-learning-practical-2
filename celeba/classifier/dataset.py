import os
from functools import partial
from typing import Any, Callable, Optional, Tuple

import PIL
import pandas
import torch
from torchvision.datasets import VisionDataset
from torchvision.datasets.utils import verify_str_arg


class CelebA(VisionDataset):
    """`Large-scale CelebFaces Attributes (CelebA) Dataset <http://mmlab.ie.cuhk.edu.hk/projects/CelebA.html>`_ Dataset.

    Args:
        root (string): Root directory where images are downloaded to.
        split (string): One of {'train', 'valid', 'test', 'all'}.
            Accordingly dataset is selected.
        target_type (string or list, optional): Type of target to use, ``attr``, ``identity``, ``bbox``,
            or ``landmarks``. Can also be a list to output a tuple with all specified target types.
            The targets represent:
                ``attr`` (np.array shape=(40,) dtype=int): binary (0, 1) labels for attributes
                ``identity`` (int): label for each person (data points with the same identity are the same person)
                ``bbox`` (np.array shape=(4,) dtype=int): bounding box (x, y, width, height)
                ``landmarks`` (np.array shape=(10,) dtype=int): landmark points (lefteye_x, lefteye_y, righteye_x,
                    righteye_y, nose_x, nose_y, leftmouth_x, leftmouth_y, rightmouth_x, rightmouth_y)
            Defaults to ``attr``. If empty, ``None`` will be returned as target.
        transform (callable, optional): A function/transform that  takes in an PIL image
            and returns a transformed version. E.g, ``transforms.ToTensor``
        target_transform (callable, optional): A function/transform that takes in the
            target and transforms it.
        download (bool, optional): If true, downloads the dataset from the internet and
            puts it in root directory. If dataset is already downloaded, it is not
            downloaded again.
    """

    base_folder = "celeba_manual"

    def __init__(
            self,
            root: str,
            split: str = "train",
            transform: Optional[Callable] = None,
            target_transform: Optional[Callable] = None,
    ) -> None:
        super(CelebA, self).__init__(root, transform=transform,
                                     target_transform=target_transform)
        self.split = split

        split_map = {
            "train": 0,
            "valid": 1,
            "test": 2,
            "all": None,
        }
        split_ = split_map[verify_str_arg(split.lower(), "split",
                                          ("train", "valid", "test", "all"))]

        fn = partial(os.path.join, self.root, self.base_folder)
        splits = pandas.read_csv(fn("list_eval_partition.txt"), header=0, index_col=0)
        attr = pandas.read_csv(fn("list_attr_celeba.txt"), header=0, index_col=0)

        mask = slice(None) if split_ is None else (splits == split_)['partition']

        self.filename = splits[mask].index.values
        self.attr = torch.as_tensor(attr[mask].values)
        self.attr = (self.attr + 1) // 2  # map from {-1, 1} to {0, 1}
        self.attr_names = list(attr.columns)

    def __getitem__(self, index: int) -> Tuple[Any, Any]:
        x = PIL.Image.open(os.path.join(self.root, self.base_folder, 'img_align_celeba',
                                        'img_align_celeba', self.filename[index]))

        target = self.attr[index, :]
        if self.transform is not None:
            x = self.transform(x)

        # if target:
        #     target = tuple(target) if len(target) > 1 else target[0]
        #
        #     if self.target_transform is not None:
        #         target = self.target_transform(target)
        # else:
        #     target = None

        return x, target

    def __len__(self) -> int:
        return len(self.attr)

    # def extra_repr(self) -> str:
    #     lines = ["Target type: {target_type}", "Split: {split}"]
    #     return '\n'.join(lines).format(**self.__dict__)
